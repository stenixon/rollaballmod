﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
	public float speed;
	public Text countText;
	public Text winText;
	public GameObject deathBall;
	public float deathBallVelocity;
	
	private Rigidbody rb;
	private int count;
	
	void Start() {
		rb = GetComponent<Rigidbody>();
		count = 0;
		SetCountText ();
		winText.text = "";
	}
	
	void FixedUpdate () {
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");
		
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		
		rb.AddForce (movement * speed);
	}
	
	
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag("Pickup")) {
			other.gameObject.SetActive(false);
			count = count + 1;
			SetCountText ();
		} else if ( (other.gameObject.CompareTag("BadPickup"))) {
			other.gameObject.SetActive(false);
			Vector3 pPos = this.transform.position;
			
			Vector3 dbPos = new Vector3 (
			pPos.x - 15,
			pPos.y + 15,
			pPos.z - 15);
			
			Instantiate(deathBall, dbPos, Quaternion.identity);
			
			
			Vector3 difference = new Vector3(
			dbPos.x + 15,
			dbPos.y - 15,
			dbPos.z + 15);
		
			Rigidbody dbBody = deathBall.GetComponent<Rigidbody>();
			dbBody.AddForce(difference * deathBallVelocity);
		}
	}
	
	void SetCountText () {
		countText.text = "Count: " + count.ToString();
		if(count >= 12) {
			winText.text = "You Win";
		}
	}
	
}